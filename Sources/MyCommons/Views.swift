//
//  File.swift
//  
//
//  Created by James Baughman on 3/14/22.
//
import Foundation
import SwiftUI

@available(iOS 14, macOS 11.0, *)
public struct NavigationLazyView<Content: View>: View {
    let build: () -> Content
    public init(_ build: @autoclosure @escaping () -> Content) {
        self.build = build
    }
    public var body: Content {
        build()
    }
}

@available(iOS 14, macOS 11.0, *)
public struct ErrorView: View {
    public init() {}
    public var errorText: String?
    
    public var body: some View {
        VStack {
            Image(systemName: "xmark.octagon")
                    .resizable()
                .frame(width: 100, height: 100, alignment: .center)
            Text(errorText!)
        }
        .padding()
        .foregroundColor(.white)
        .background(Color.red)
    }
    public init(errorText: String) {
        self.errorText = errorText
    }
}
