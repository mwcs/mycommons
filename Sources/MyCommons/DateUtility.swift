import Foundation

/// Alllows to get only what date information you want from a date string
/// - Parameters:
///   - currentDateFormate: Alllows to get only what date information you want from a date string
///   - conVertFormate: yyyy-MM-dd'T'HH:mm:ssZ
///   - convertDate: MM
/// - Returns: Returns the srting bassed on convertDate value
public func GetOnlyDateMonthYearFromFullDate(currentDateFormate: String, conVertFormate: String, convertDate: String) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = currentDateFormate
    guard let finalDate = formatter.date(from: convertDate) else { formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"; let second = formatter.date(from: convertDate); formatter.dateFormat = conVertFormate; return formatter.string(from: second!) }
    formatter.dateFormat = conVertFormate
    let dateString = formatter.string(from: finalDate)

    return dateString
}

// Returns the current month
public func getCurrentMonth() -> String {
    let current = Calendar.current
    let monthNO = current.component(.month, from: Date())
    let month = current.monthSymbols[monthNO - 1]
    return month
}

// this addes get first or last day of current month or next month
public extension Date {
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }

    func startOfNextMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.date(byAdding: .month, value: 1, to: Calendar.current.startOfDay(for: self))!))!
    }

    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: startOfMonth())!
    }

    func endOfNextMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: startOfNextMonth())!
    }
    
    func lastYear() -> Date {
        return Calendar.current.date(byAdding: DateComponents(year: -1), to: Date())!
    }
}

public extension Date {
    func MyDateFormat(date: Date) -> String {
        return DateFormatter.shortDate.string(from: date)
    }
}

public extension DateFormatter {
    static let shortDate: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.isLenient = false
        return dateFormatter
    }()
}

// Check if weekemd returns the day int
// Change to return true or false if 6 or 7
public func checkIfWeekend() -> Int {
    let today = Date()
    let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
    let dayNumber = calendar!.components([.weekday], from: today as Date)
    return dayNumber.weekday!
}

/// Check if current time is in set range
/// - Parameters:
///   - startHour: Start hour would be something like 5 for 5 am.  Range is 0 - 23
///   - startMin: Start minitue range is 0 - 59
///   - startSecond: Start second range is 0 - 59
///   - endHour: End hour would be somthing like 20 for 8 pm. Range is 0 - 23
///   - endMin: End minitue range is 0 - 59
///   - endSecond: End second range is  0 - 59
/// - Returns: Returns true if time is betwwen start and end times.
public func checkIfInTimeRange(startHour: Int, startMin: Int, startSecond: Int, endHour: Int, endMin: Int, endSecond: Int) -> Bool {
    let calendar = Calendar.current
    let now = Date()
    let nowDateValue = now as Date
    let todayStartTime = calendar.date(bySettingHour: 5, minute: 0, second: 0, of: nowDateValue)
    let todayEndTime = calendar.date(bySettingHour: 23, minute: 0, second: 0, of: nowDateValue)
    if nowDateValue >= todayStartTime!, nowDateValue <= todayEndTime! { return true }
    return false
}

/// Check if  date is older than two months.
/// - Parameter epocdate: This accetps EPOC UTC time
///- Returns: Returns Bool
///
/// The later variable uses `-5259492` to subtact two months from the time given to see if it's older than two months.
/// The Bool returns false if date is not older than two months.  If the date is odler than two moths it rerurnts true.

public func twomonths(epocdate: Int) -> Bool {
    let eventDate = Date(timeIntervalSince1970: TimeInterval(epocdate) / 1000)
    let now = Date()
    let later = Date().addingTimeInterval(-5259492)
    let range = later ... now
    if range.contains(eventDate) {
        print("Yup date is good")
        return false
    } else {
        print("Nope date is bad")
        return true
    }
}

// Date compare
public func checkCreateUpdate(date1: Int64, date2: Int64) -> Bool {
        if date1 == date2 {
            return true
        } else {
            return false
        }
    }

@available(iOS 13.0, *)
public extension Date {
    /// Converts time to something like 2 mintues, 1 hr ago.
    /// - Returns: Rurtns `String` of example  "2 minutes"
    func timeAgoDisplay() -> String {
        let formatter = RelativeDateTimeFormatter()
        formatter.unitsStyle = .full
        return formatter.localizedString(for: self, relativeTo: Date())
    }
}
