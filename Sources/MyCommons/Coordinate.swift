//
//  File.swift
//  
//
//  Created by James Baughman on 2/23/22.
//
// Swift Convert decimal coordinate into degrees, minutes, seconds, direction
import Foundation
import CoreLocation

extension BinaryFloatingPoint {
    var dms: (degrees: Int, minutes: Int, seconds: Int) {
        var seconds = Int(self * 3600)
        let degrees = seconds / 3600
        seconds = abs(seconds % 3600)
        return (degrees, seconds / 60, seconds % 60)
    }
}

extension CLLocation {
    var dms: String { latitude + " " + longitude }
    var latitude: String {
        let (degrees, minutes, seconds) = coordinate.latitude.dms
        return String(format: "%d°%d'%d\"%@", abs(degrees), minutes, seconds, degrees >= 0 ? "N" : "S")
    }
    var longitude: String {
        let (degrees, minutes, seconds) = coordinate.longitude.dms
        return String(format: "%d°%d'%d\"%@", abs(degrees), minutes, seconds, degrees >= 0 ? "E" : "W")
    }
}

/// Example usage:
/// let latitude = -22.9133950
/// let longitude = -43.2007100
/// let location = CLLocation(latitude: latitude, longitude: longitude)
/// location.latitude  // "22°54'48"S"
/// location.longitude // "43°12'2"W"
/// location.dms       // "22°54'48"S 43°12'2"W"
