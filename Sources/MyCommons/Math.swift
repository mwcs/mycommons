//
//  Math.swift
//  
//
//  Created by James Baughman on 5/31/22.
//

import Foundation

/// Helps get average tempature
/// - Parameter tempArray: Provice an Arry of Doubles
/// - Returns: Returnes a String with one decimal place. 
public func avgTemp(tempArray: [Double]) -> String {
    let sumArray =  tempArray.reduce(0, +)
    let avgTemp = Double(sumArray) / Double(tempArray.count)
    let avgTempRounded = avgTemp.roundToDecimal(1)
    return String(format:"%.1f", avgTempRounded)
}
