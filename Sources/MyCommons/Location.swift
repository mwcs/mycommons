//
//  File.swift
//  
//
//  Created by James Baughman on 2/24/22.
//

import Foundation
import CoreLocation
import SwiftUI
import Combine

#if os(iOS)
public let defaultAuthorizationRequestType = CLAuthorizationStatus.authorizedWhenInUse
#else
public let defaultAuthorizationRequestType = CLAuthorizationStatus.authorizedAlways
#endif

@available(iOS 14.0, *)
public class LocationViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
    @Published public var authorizationStatus: CLAuthorizationStatus
    public let locationManager: CLLocationManager
    
    public override init() {
        locationManager = CLLocationManager()
        authorizationStatus = locationManager.authorizationStatus
        
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
   public func requestPermission() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    public func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        authorizationStatus = manager.authorizationStatus
    }
    
    public func requestAuthorization(authorizationRequestType: CLAuthorizationStatus = defaultAuthorizationRequestType) -> Void {
            if self.authorizationStatus == CLAuthorizationStatus.denied {
//                onAuthorizationStatusDenied()
                print("need auth")
            }
            else {
                switch authorizationRequestType {
                case .authorizedWhenInUse:
                    self.locationManager.requestWhenInUseAuthorization()
                case .authorizedAlways:
                    self.locationManager.requestAlwaysAuthorization()
                default:
                    print("WARNING: Only `when in use` and `always` types can be requested.")
                }
            }
        }
}


