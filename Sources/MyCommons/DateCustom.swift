//
//  DateCustom.swift
//  Pool Maintance
//
//  Created by James Baughman on 5/29/20.
//  Copyright © 2020 James Baughman. All rights reserved.
//

import SwiftUI
import Combine

struct DateCustom: View {
    var body: some View {
        VStack {
            Home()
        }
    }
}

struct DateCustom_Previews: PreviewProvider {
    static var previews: some View {
        DateCustom()
    }
}


struct Home: View {
    @State var date = Date()
    @State var data: DateType!
    @State var expand = false
    @State var year = false
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                if self.data != nil {
                    
                    ZStack {
                        VStack(spacing: 15) {
                            ZStack {
                                HStack {
                                    Spacer()
                                    Text(self.data.Month)
                                        .font(.title)
                                        .foregroundColor(.white)
                                    Spacer()
                                }
                                .padding(.vertical)
                                HStack {
                                    Button(action: {
                                        self.date = Calendar.current.date(byAdding: .month, value: -1, to: self.date)!
                                        self.UpdateDate()
                                    }) {
                                        Image(systemName: "arrow.left")
                                    }
                                    Spacer()
                                    Button(action: {
                                        self.date = Calendar.current.date(byAdding: .month, value: 1, to: self.date)!
                                        self.UpdateDate()
                                    }) {
                                        Image(systemName: "arrow.right")
                                    }
                                }
                                .padding(.horizontal, 30)
                                .foregroundColor(.white)
                            }
                            .background(Color.red)
                            Text(self.data.Date)
                                .font(.system(size: 45))
                                .fontWeight(.bold)
                            Text(self.data.Day)
                                .font(.title)
                            Divider()
                            ZStack {
                                Text(self.data.Year)
                                    .font(.title)
                                
                                HStack {
                                    Spacer()
                                    Button(action: {
                                        withAnimation(.default) {
                                            self.expand.toggle()
                                        }
                                    }) {
                                        Image(systemName: "arrow.right")
                                            .renderingMode(.original)
                                            .resizable()
                                            .frame(width: 10, height: 16).rotationEffect(.init(degrees: self.expand ? 270 : 90))
                                    }.padding(.trailing, 30)
                                }
                            }
                            
                            if self.expand {
                                Toggle(isOn: self.$year) {
                                    Text("Year")
                                        .font(.title)
                                }
                                .padding(.trailing, 15)
                                .padding(.leading, 25)
                            }
                        }
                        .padding(.bottom, self.expand ? 10 : 12)
                        
                        
                        HStack {
                            Button(action: {
                                self.date = Calendar.current.date(byAdding: self.year ? .year : .day, value: -1, to: self.date)!
                                self.UpdateDate()
                            }) {
                                Image(systemName: "arrow.left")
                            }
                            Spacer()
                            Button(action: {
                                self.date = Calendar.current.date(byAdding: self.year ? .year : .day, value: 1, to: self.date)!
                                self.UpdateDate()
                            }) {
                                Image(systemName: "arrow.right")
                            }
                        }
                        .padding(.horizontal, 30)
                        .foregroundColor(.black)
                    }
                }
            }
            .frame(width: geometry.size.width / 1)
            .background(Color.white)
            .cornerRadius(15)
        }
        .background(Color.black.opacity(0.06))
//        .edgesIgnoringSafeArea(.all)
        .onTapGesture {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-YYYY"
//            print(formatter.string(from: self.date))
        }
        .onAppear {
            self.UpdateDate()
        }
    }
    
    func UpdateDate() {
        let current = Calendar.current
        let date = current.component(.day, from: self.date)
        let monthNO = current.component(.month, from: self.date)
        let month = current.monthSymbols[monthNO - 1]
        let year = current.component(.year, from: self.date)
        let weekNO = current.component(.weekday, from: self.date)
        let day = current.weekdaySymbols[weekNO - 1]
        self.data = DateType(Day: day, Date: "\(date)", Year: "\(year)", Month: month)
    }
}

struct DateType {
    var Day: String
    var Date: String
    var Year: String
    var Month: String
}
