//
//  File.swift
//  
//
//  Created by James Baughman on 4/15/22.
//

import Foundation
import SwiftUI

public struct GrowingButton: ButtonStyle {
    public func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding()
            .background(Color.blue)
            .foregroundColor(Color.white)
            .clipShape(Capsule())
            .scaleEffect(configuration.isPressed ? 1.2 : 1)
            .animation(.easeOut(duration: 0.2), value: configuration.isPressed)
    }
}

public struct SwipeButtonStyle: PrimitiveButtonStyle {
    public func makeBody(configuration: Configuration) -> some View {
        Button(configuration)
            .gesture(
                DragGesture()
                    .onEnded { _ in
                        configuration.trigger()
                    }
            )
            .padding()
            .background(Color.yellow.cornerRadius(10))
    }
}
