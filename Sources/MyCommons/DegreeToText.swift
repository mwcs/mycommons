//
//  File.swift
//  
//
//  Created by James Baughman on 2/23/22.
//

import Foundation

///  Used to convert compass degree to direction.
///
///   When you need to convert compass heading from a degree 0 to 359 to a direction like N, S, NNE, ESE.
///
///   ```Swift
///   let direction = degreeToText(degree: 180)
///   print(direction)
///   ```
///
///   Would print S.
///
/// - Parameter degree: 180
/// - Returns: A string like S for south
public func degreeToText(degree: Double) -> String {
    let val = ((degree / 22.5) + 0.5);
    let arr = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"];
    return arr[Int(val.truncatingRemainder(dividingBy: 16))]
}

