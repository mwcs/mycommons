//
//  extension.swift
//  
//
//  Created by James Baughman on 3/14/22.
//

import Foundation

public extension Array where Element: Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        return result
    }
}


public extension String {
    /// Convert a set of numbner using a pattern to a new foramt.
    /// - Parameters:
    ///   - pattern: Could be like # (###) ###-####
    ///   - replacementCharacter: Defining the `#` charcter used in the patteren.
    /// - Returns: Returns the formatted stting in the desired format.
    ///
    /// Lets say you have a phone numbner that looks like  this "+19701112222"  and you want to look like this "+1 (970) 111-2222".  Define your pattern and then make sure set the replacementCharter to whatever you used in the pattern. 
    ///
    func applyPatternOnNumbers(pattern: String, replacementCharacter: Character) -> String {
        var pureNumber = self.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        for index in 0 ..< pattern.count {
            guard index < pureNumber.count else { return pureNumber}
            let stringIndex = String.Index(utf16Offset: index, in: pattern)
            let patternCharacter = pattern[stringIndex]
            guard patternCharacter != replacementCharacter else { continue }
            pureNumber.insert(patternCharacter, at: stringIndex)
        }
        return pureNumber
    }
}

extension Double {
    /// Alllows you to select how many decimal placese to round to.
    /// - Parameter fractionDigits: Number of decimails places
    /// - Returns: Returned Double
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
}
