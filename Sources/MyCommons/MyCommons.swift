
import Foundation
import MapKit

public func unixToDate(unix: Double) -> String {
    let date = Date(timeIntervalSince1970: unix)
    let dateFormatter = DateFormatter()
    dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
    dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
    dateFormatter.timeZone = .current
    let localDate = dateFormatter.string(from: date)
    return localDate
}


public class CheckPoly {
   public func checkInPoly(lat: Double, long: Double, filterCounty: [CLLocationCoordinate2D]) -> Bool {
        let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
        var points = filterCounty
        let polygon = MKPolygon(coordinates: &points, count: points.count)
        let polygonRenderer = MKPolygonRenderer(polygon: polygon)
        let mapPoint: MKMapPoint = MKMapPoint(location)
        let polygonViewPoint: CGPoint = polygonRenderer.point(for: mapPoint)
        if polygonRenderer.path.contains(polygonViewPoint) {
            return true
        }
        return false
    }
    
    public func checkInCircle(centerCoordinate: CLLocationCoordinate2D, radius: Double, point: CLLocationCoordinate2D) -> Bool {
        let camPoint: MKMapPoint = MKMapPoint(point)
        let circle = MKCircle(center: centerCoordinate, radius: radius)
        if circle.boundingMapRect.contains(camPoint) {
            return true
        }
        return false
    }
}




//public func getCountyFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) -> String {
//    print("lat: \(pdblLatitude) - long: \(pdblLongitude)")
//    var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
////    let lat: Double = Double("\(pdblLatitude)")!
////    //21.228124
////    let lon: Double = Double("\(pdblLongitude)")!
////    //72.833770
//    let ceo: CLGeocoder = CLGeocoder()
//    center.latitude = pdblLatitude
//    center.longitude = pdblLongitude
//    
//    let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
//    
//    var county = "Not Set"
//    DispatchQueue.main.async {
//        ceo.reverseGeocodeLocation(loc, completionHandler: {(placemarks, error) in
//            if (error != nil)
//            {
//                print("reverse geodcode fail: \(error!.localizedDescription)")
//            }
//            let pm = placemarks! as [CLPlacemark]
//
//            if pm.count > 0 {
//
//                let pm = placemarks![0]
//                let adminarea = pm.subAdministrativeArea
//                if let index = (adminarea!.range(of: " ")?.lowerBound)
//                {
//                    //prints "County name"
//                    let beforeEqualsTo = String(adminarea!.prefix(upTo: index))
//                    county = beforeEqualsTo
//                }
//            }
//        })
//    }
//    
//
//    print("The county is: \(county)")
//    return county
//}


/// Triming string based on a charctor
/// - Parameters:
///   - modstring: String to parse
///   - charactorRemoveAfter: Like " " or "="
///   - type: afterEqualsTo, beforeEqualsToContainingSymbol, afterEqualsToContainingSymbol, beforeEqualsTo
/// - Returns: Returns the desired string
public func trimString(modstring: String, charactorRemoveAfter: String, type: String) -> String {
    
    if let index = (modstring.range(of: charactorRemoveAfter)?.upperBound)
    {
        switch type {
        case "afterEqualsTo":
            //prints "value"
            let afterEqualsTo = String(modstring.suffix(from: index))
            print(afterEqualsTo)
            return afterEqualsTo
        case "beforeEqualsToContainingSymbol":
            //prints "element="
            let beforeEqualsToContainingSymbol = String(modstring.prefix(upTo: index))
              print(beforeEqualsToContainingSymbol)
            return beforeEqualsToContainingSymbol
        default:
            print("nope")
            return "Nope"
        }
    }

    if let index = (modstring.range(of: charactorRemoveAfter)?.lowerBound)
    {
        switch type {
        case "afterEqualsToContainingSymbol":
            //prints "=value"
            let afterEqualsToContainingSymbol = String(modstring.suffix(from: index))
              print(afterEqualsToContainingSymbol)
            return afterEqualsToContainingSymbol
        case "beforeEqualsTo":
            //prints "element"
            let beforeEqualsTo = String(modstring.prefix(upTo: index))
              print(beforeEqualsTo)
            return beforeEqualsTo
        default:
            return "Nope"
        }
    }
    return "No Value"
}

public func calcPercentage(value: Double) -> String {
    let calc = value * 100
    return String(format: "%.0f", calc)
}

public func zeroDecimal(value: Double) -> String {
    return String(format: "%.0f", value)
}

public func twoDecimal(value: Double) -> String {
    return String(format: "$.2f", value)
}

public func percentageUsed(total: Double, used: Double) -> String {
    let calc = used / total
    let percent = calc * 100
    return String(format: "%.0f", percent)
}

