# MyCommons

Make sure to `import MyCommons` to use any of the functions. 

When using the `removeDuplicates()` from the array extention make sure to follow this format.

`YourArray = YourArray.removeDuplicates()`

If you use `_ = YourArray.removeDuplicates()` to silnece the unused results the duplicates will not be removed. 
